package;

class BoxAction {

  private function new() {}
  
  public function execute(root: Box, x: Int, y: Int) {}
  
  public function show(root: Box) {}
 
  public function isMine(): Bool { return false; }
}