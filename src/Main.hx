package;

import openfl.display.Sprite;
import openfl.events.Event;
import openfl.Lib;
import openfl.Assets;
import openfl.media.Sound;
import openfl.media.SoundChannel;

class Main extends Sprite {

	public function new() {
		super();
    Lib.current.stage.align = openfl.display.StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = openfl.display.StageScaleMode.NO_SCALE;
    Lib.current.addChild(Board.getInstance());
    
    var music: Sound = Assets.getMusic("snd/music.ogg");
    var channel: SoundChannel = music.play(0, 1000);
	}

}
