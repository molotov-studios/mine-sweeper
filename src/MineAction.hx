package;

import Box.BoxState;

class MineAction extends BoxAction {

  public function new() {
    super();
  }
  
  public override function execute(root: Box, x: Int, y: Int) {
    root.setImage("img/mine_exploded.png");
    root.setState(BoxState.OPEN);
    Board.getInstance().endGame();
  }
  
  public override function show(root: Box) {
    root.setImage("img/mine.png");
    root.setState(BoxState.OPEN);
  }
  
  public override function isMine(): Bool { 
    return true;
  }
  
}