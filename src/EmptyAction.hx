package;

class EmptyAction extends BoxAction
{

  public function new() {
    super();
  }
  
  public override function execute(root: Box, x: Int, y: Int) {
    Board.getInstance().propagateOpen(x, y);
  }
  
}