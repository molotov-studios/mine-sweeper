package;

import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.Assets;


enum BoxState {
  OPEN;
  CLOSE;
  FLAG;
}

class Box extends Sprite {
  
  private var _action: BoxAction = null;
  private var _state: BoxState = BoxState.CLOSE;
  
  private var _pos_x: Int;
  private var _pos_y: Int;
  
  private var _size: Float;
  
  private var _background: Bitmap = null;

  public function new(y: Int, x: Int, size: Float) {
    
    super();
    
    _pos_x = x;
    _pos_y = y;
    _size = size;
    
    this.x = x * size;
    this.y = y * size;
    
    setImage("img/unknown.png");
    
    _action = new EmptyAction();
    
    this.addEventListener(MouseEvent.CLICK, onMouseEvent);
    this.addEventListener(MouseEvent.RIGHT_CLICK, onMouseEvent);
  }
  
  public function setMine(): Bool {
    
    if (Type.getClass(_action) == MineAction) 
      return false;
    
    _action = new MineAction();
    return true;
  }
  
  public function setImage(img_name: String) {
    
    if(_background != null)
      this.removeChild(_background);
    
    _background = new Bitmap(Assets.getBitmapData(img_name));
    this.addChild(_background);
    _background.width = _background.height = _size;
  }
  
  public function removeEventListeners() {
    
    this.removeEventListener(MouseEvent.CLICK, onMouseEvent);
    this.removeEventListener(MouseEvent.RIGHT_CLICK, onMouseEvent);
  }
  
  public function isMine() {
    return _action.isMine();
  }
  
  public function show() {
    
    if (_state == BoxState.OPEN)
      return;
    
    _action.show(this);
  }
  
  private function onMouseEvent(event: MouseEvent) {
    
    if (_state == BoxState.OPEN)
      return;
    
    switch(event.type) {
      
      case MouseEvent.CLICK:
        if (_state == BoxState.CLOSE)
          _action.execute(this, _pos_x, _pos_y);
      
      case MouseEvent.RIGHT_CLICK:
        if (_state == BoxState.FLAG) {
          setState(BoxState.CLOSE);
          setImage("img/unknown.png");
        }
        else {
          setState(BoxState.FLAG);
          setImage("img/flag.png");
        }
    }

  }
  
  public function getState(): BoxState {
    return _state;
  }
  
  public function setState(state: BoxState) {
    _state = state;
  }
}