package;

import openfl.display.Sprite;
import openfl.Lib;
import openfl.text.TextField;
import Box.BoxState;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;

class Board extends Sprite {
  
  private static inline var N_MINES: Int = 10;
  private static inline var N_ROWS: Int = 9;
  private static inline var N_COLS: Int = 9;
  
  private static inline var BOARD_MAX_SIZE_RATIO: Float = 0.8;
  
  private var _rows: Int;
  private var _cols: Int;
  private var _n_mines: Int;
  private var _box_size: Float;
  
  private var _n_opened_boxes: Int = 0;
  
  private var _boxes: Array< Array< Box > >;
  
  private static var _instance: Board = null;
  
  public static function getInstance(): Board {
    if (_instance == null)
      _instance = new Board(N_ROWS, N_COLS, N_MINES);
    return _instance;
  }

  private function new(rows: Int, cols: Int, n_mines: Int): Void 
  {
    
    super();
    
    _rows = rows;
    _cols = cols;
    _n_mines = n_mines;
    
    _boxes = new Array<Array<Box>>();
    
    _box_size = Math.min((0.8 * Lib.current.stage.stageWidth) / N_COLS, (0.8 * Lib.current.stage.stageHeight) / N_ROWS);
    this.x = (Lib.current.stage.stageWidth - (_box_size * N_COLS)) / 2.0;
    this.y = (Lib.current.stage.stageHeight - (_box_size * N_ROWS)) / 2.0;
    
    for (i in 0..._rows) {
      
      _boxes.push( new Array<Box>() );
      
      for (j in 0..._cols) {
        
        _boxes[i].push(new Box(i, j, _box_size));
        this.addChild(_boxes[i][j]);
      }
    }
    
    var mines_on_board = 0;
    
    while (mines_on_board < n_mines) {
      
      if (_boxes[Std.random(_rows)][Std.random(_cols)].setMine())
        ++mines_on_board;
    }
  }
  
  public function endGame(show_mines: Bool = true) {
    
    for (i in 0..._rows) {
      for (j in 0..._cols) {
        
        _boxes[i][j].removeEventListeners();
        if (show_mines)
          _boxes[i][j].show();
      }
    }
  }
  
  public function checkVictory() {
    
    if (_n_opened_boxes == ((_rows * _cols) - _n_mines)) {
      trace ("¡Victoria!");
      return true;
    }
    return false;
  }
  
  public function propagateOpen(x: Int, y: Int) {
    
    if (y < 0 || y >= _rows || x < 0 || x >= _cols || _boxes[y][x].getState() == BoxState.OPEN)
      return;
    
    _boxes[y][x].setState(BoxState.OPEN);
    _boxes[y][x].setImage("img/empty.png");
    
    ++_n_opened_boxes;
    
    var n_mines: Int = 0;
    
    for (i in -1...2)
      for (j in -1...2)
        if (i != 0 || j != 0)
          if ((y + i) >= 0 && (y + i) < _rows && (x + j) >= 0 && (x + j) < _cols)
            n_mines += _boxes[y + i][x + j].isMine()? 1: 0;
    
    if (n_mines == 0) {
      for (i in -1...2)
        for (j in -1...2)
          if (i != 0 || j != 0)
            propagateOpen(x + j, y + i);
    }
    else {
      createText(_boxes[y][x], n_mines);
    }
    
    if (checkVictory()) {
      Board.getInstance().endGame(false);
      return;
    }
    
  }
  
  private function createText(box: Box, n_mines: Int) {
    var number = new TextField();
    number.text = Std.string(n_mines);
    number.width = _box_size;
    number.y = (number.textHeight) / 2.0;
    number.defaultTextFormat = new TextFormat("_sans", 22, Math.round((0xFF0000 - 0x0000FF) * (n_mines - 1) / 4 + 0x0000FF), true, null, null, null, null, TextFormatAlign.CENTER);
    box.addChild(number);
  }
}