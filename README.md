# MineSweeper in OpenFL

## Summary

Simple implementation of the well-kown game *MineSweeper*.

## UML

This UML class diagram represents the structure of the code:

![](dev/uml.png)

## Installation

Just clone this repository and use lime or openfl to build it:

````sh
lime build windows
````

The full project is located at `master` branch, while the template one is located at `incomplete` branch.

## Versions

The following versions of haxelib libraries are being used on this project:

- **Haxe:** 3.1.3
- **OpenFL:** 3.5.2
- **Lime:** 2.8.1
- **Actuate:** 1.8.6

## Authors ##

This project has been developed by:

| Avatar | Name | Nickname | Email |
| ------- | ------------- | --------- | ------------------ |
| ![](http://www.gravatar.com/avatar/2ae6d81e0605177ba9e17b19f54e6b6c.jpg?s=64)  | Daniel Herzog | Wikiti | [wikiti.doghound@gmail.com](mailto:wikiti.doghound@gmail.com) |
| ![](https://avatars0.githubusercontent.com/u/6042873?v=3&s=64)  | Cristo González | Shylpx | [cristogr.93@gmail.com](mailto:cristogr.93@gmail.com) |
| ![](https://cursurideactorie.files.wordpress.com/2010/10/unknown-person.gif =64x64)  | Cristian Luis | Crispian | [Crispilandia@gmail.com](mailto:Crispilandia@gmail.com) |
